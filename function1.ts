function getTime(){
    return new Date().getTime();
}

console.log(getTime);

function printHello(): void {
    console.log("Hello");
}

printHello();

function multiply(a:number ,b :number) : number{
    return a*b;
}

console.log(multiply(3,5));

function add(a:number,b :number,c? :number):number{
    return a*b*(c || 0);
}
console.log(add(3,8,9));

function pow (value:number,exponent:number =10) : number{
    return value ** exponent;
}

console.log(pow(10,10));
console.log(pow(10,2)); 

function divide({divide,dividsor}:{divide:number,dividsor:number}){
    return divide/dividsor;
}

console.log(divide({divide:100,dividsor:10}));

function add2(a: number, b: number, ...rest: number[]) {
    return a + b + rest.reduce((p, c) => p + c, 0);
  }

  console.log(add2(1,2,3,4,5));

  type Negate = (value: number) => number;
  const negateFunction: Negate = (value) => value * -1;
  const negateFunction2: Negate = function(value : number) :number {return value * -1};
  console.log(negateFunction(1));
  console.log(negateFunction2(10));